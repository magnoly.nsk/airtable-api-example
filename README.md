* Подключиться к Airtable через Retrofit2
* Конфигурацию на HOCON
* Скачать обратную связь
* Построить рэйтинг докладов  
нормировать по числу оценок
https://en.wikipedia.org/wiki/Bayesian_average
* Вытащить все комментарии по группам

Download JDK13 from https://jdk.java.net/13/
 
Update Gradle to 6:
````
./gradlew wrapper --gradle-version=6.2.1 --distribution-type=bin
````
Build:
````
./gradlew assemble -Dorg.gradle.java.home=/usr/lib/jvm/openjdk13
````
Run:
````
./gradlew run -Dorg.gradle.java.home=/usr/lib/jvm/openjdk13
````
