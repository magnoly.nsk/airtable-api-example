package org.mgaidamak.data;

public class Aggregate {
    private String name;
    private double rating;
    private int[] rates;
    private double level;
    private int[] levels;
    private long total;
    long afterEnd;

    public Aggregate(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public int[] getRates() {
        return rates;
    }

    public void setRates(int[] rates) {
        this.rates = rates;
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public int[] getLevels() {
        return levels;
    }

    public void setLevels(int[] levels) {
        this.levels = levels;
    }

    public long getAfterEnd() {
        return afterEnd;
    }

    public void setAfterEnd(long afterEnd) {
        this.afterEnd = afterEnd;
    }
}
