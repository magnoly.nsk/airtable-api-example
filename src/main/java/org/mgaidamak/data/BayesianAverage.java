package org.mgaidamak.data;

import java.util.Arrays;

/**
 * https://en.wikipedia.org/wiki/Bayesian_average
 */
public class BayesianAverage {
    private double c;
    private double m;

    public BayesianAverage(double c, double m) {
        this.c = c;
        this.m = m;
    }

    public double average(int[] rates) {
        return (c*m + Arrays.stream(rates).sum()) / (c + Arrays.stream(rates).count());
    }
}
