package org.mgaidamak;

import com.google.gson.Gson;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.mgaidamak.api.*;
import org.mgaidamak.data.Aggregate;
import org.mgaidamak.data.BayesianAverage;

import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Config conf = ConfigFactory.load();
//        String apiKey = System.getenv("apiKey");
        String apiKey = "xxx";
        System.out.println(apiKey);

        HttpClient httpClient = HttpClient.newHttpClient();
        Gson gson = new Gson();

        List<Aggregate> aggregates = new ArrayList<>();

        List<String> tables = conf.getStringList("tables");

        // day of feedback request
        Instant endOfConf = Instant.parse("2019-12-02T00:00:00.000Z");
        try {
            Files.createDirectories(Paths.get("out", "notes"));
        } catch (IOException ioe) {
            System.out.println("Can't create notes path. " + ioe.getMessage());
            return;
        }

        for (String table : tables) {
            try (FileWriter fw = new FileWriter("out/notes/" + table + ".txt")) {
                HttpRequest request = getHttpRequest(apiKey, table);

                String body = httpClient.send(request, HttpResponse.BodyHandlers.ofString()).body();
                Results results = gson.fromJson(body, Results.class);

                if (results != null) {
                    Aggregate aggregate = new Aggregate(table);
                    long total = Arrays.stream(results.getRecords()).count();
                    if (total == 0) {
                        System.out.println("Skip '" + table + "' (has no feedback)");
                        continue;
                    }
                    aggregate.setTotal(total);
                    int[] rates = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().getRating() != null)
                            .mapToInt(r -> r.getFields().getRating())
                            .toArray();
                    int[] levels = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().getLevel() != null)
                            .mapToInt(r -> r.getFields().getLevel())
                            .toArray();
                    long afterEnd = Arrays.stream(results.getRecords())
                            .map(Record::getFields)
                            .filter(Objects::nonNull)
                            .map(Fields::getDate)
                            .filter(Objects::nonNull)
                            .filter(i -> i.isAfter(endOfConf))
                            .count();
                    String interests = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().getInterest() != null)
                            .flatMap(r -> Arrays.stream(r.getFields().getInterest()))
                            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                            .entrySet()
                            .stream()
                            .map(r -> r.getKey().getDescription() + ": " + r.getValue() + " votes")
                            .collect(Collectors.joining("\n"));
                    long video = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().isVideo())
                            .count();
                    long english = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().isEnglish())
                            .count();
                    long more = Arrays.stream(results.getRecords())
                            .filter(r -> r.getFields() != null)
                            .filter(r -> r.getFields().isMore())
                            .count();

                    aggregate.setRates(rates);
                    aggregate.setLevels(levels);
                    aggregate.setAfterEnd(afterEnd);

                    aggregates.add(aggregate);

                    String notes = Arrays.stream(results.getRecords())
                            .flatMap(r -> Stream.of(r.getFields()))
                            .filter(f -> f.getNote() != null)
                            .map(f -> Optional.ofNullable(f.getEmail()).orElse("Anonymous") + "\n" + f.getNote())
                            .collect(Collectors.joining("\n\n"));

                    fw.append("Сколько человек оставили обратную связь?\n");
                    fw.append("How many people left feedback?\n");
                    fw.append("" + total + "\n\n");

                    fw.append("Общая оценка для рейтинга докладов.\n");
                    fw.append("Rating from 1 bad :( to 5 amazing :)\n");
                    fw.append(String.format("Average: %.1f\n\n", Arrays.stream(rates).average().orElse(0)));

                    fw.append("Насколько доклад был интересен для вас?\n");
                    fw.append("How interesting was the report for you?\n");
                    fw.append(interests + "\n\n");

                    fw.append("Насколько сложно было?\n");
                    fw.append("How difficult was it? 1 - smoothie, 3 - hard-core\n");
                    fw.append(String.format("Average: %.1f\n\n", Arrays.stream(levels).average().orElse(0)));

                    fw.append("Если на английском, то понятно было?\n");
                    fw.append("Are you understand English/Russian?\n");
                    fw.append("" + english + " people answered Yes.\n\n");

                    fw.append("Будете ли пересматривать запись?\n");
                    fw.append("Will you review the recording?\n");
                    fw.append("" + video + " people answered Yes.\n\n");

                    fw.append("Остались вопросы к докладчику?\n");
                    fw.append("Still have questions for the speaker?\n");
                    fw.append("" + more + " people answered Yes.\n\n");

                    fw.append("Что передать докладчику?\n");
                    fw.append("Send message to speaker?\n\n");
                    fw.append(notes);
                } else {
                    System.out.println("Skip '" + table + "' (has null feedback)");
                }
            } catch (Exception e) {
                e.printStackTrace(System.out);
            }
        }

        // rates per one talk
        IntSummaryStatistics rateSmr = aggregates.stream()
                .mapToInt(a -> a.getRates().length)
                .summaryStatistics();
        // rates of all talks
        IntSummaryStatistics ratesSmr = aggregates.stream()
                .flatMapToInt(a -> Arrays.stream(a.getRates()))
                .summaryStatistics();

        // Bayesian Average calculator
        BayesianAverage rateBa = new BayesianAverage(rateSmr.getAverage(), 3);

        System.out.println("Rates per one talk: " + rateSmr.toString());
        System.out.println("Total rates: " + ratesSmr.toString());
        System.out.println("Rating");
        aggregates.stream()
                .peek(a -> a.setRating(rateBa.average(a.getRates())))
                .sorted(Comparator.comparingDouble(Aggregate::getRating).reversed())
                .forEach(r -> System.out.println(String.format("%.2f %s (%d votes)",
                        r.getRating(), r.getName(), Arrays.stream(r.getRates()).count())));

        long after7dec = aggregates.stream().mapToLong(Aggregate::getAfterEnd).sum();
        System.out.println("Feedback after end of conference: " + after7dec);
    }

    /**
     *
     * @param aiKey api key
     * @param table table name (user friendly)
     * @return http request
     */
    private static HttpRequest getHttpRequest(String aiKey, String table) {
        var encoded = URLEncoder.encode(table, StandardCharsets.UTF_8);
        var str = "https://api.airtable.com/v0/appto6JTuMoQ2ZC5Y/" + encoded;
        return HttpRequest.newBuilder(URI.create(str).normalize())
                .header("Accept", "application/json")
                .header("Authorization", "Bearer " + aiKey)
                .build();
    }
}
