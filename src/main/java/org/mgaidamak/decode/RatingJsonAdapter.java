package org.mgaidamak.decode;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

public class RatingJsonAdapter extends TypeAdapter<Integer> {

    @Override
    public void write(JsonWriter out, Integer value) throws IOException {
        out.value(Integer.toString(value));
    }

    @Override
    public Integer read(JsonReader in) throws IOException {
        String value = in.nextString();
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException ne) {
            return 0;
        }
    }
}
