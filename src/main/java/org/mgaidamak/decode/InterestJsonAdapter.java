package org.mgaidamak.decode;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import org.mgaidamak.api.Interest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class InterestJsonAdapter extends TypeAdapter<Interest[]> {

    @Override
    public void write(JsonWriter out, Interest[] values) throws IOException {
        out.beginArray();
        for (Interest value: values) {
            out.value(Optional.ofNullable(value)
                    .map(Interest::getDescription)
                    .orElse(null));
        }
        out.endArray();
    }

    @Override
    public Interest[] read(JsonReader in) throws IOException {
        ArrayList<Interest> interests = new ArrayList<>();

        in.beginArray();
        while (in.hasNext()) {
            Interest interest = Interest.fromString(in.nextString());
            if (interest != null) {
                interests.add(interest);
            }
        }
        in.endArray();

        return interests.toArray(new Interest[0]);
    }
}
