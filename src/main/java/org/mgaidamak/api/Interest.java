package org.mgaidamak.api;

public enum Interest {
    work("для работы",
            "Полезно для работы (Useful for work)"),
    pet("для личных проектов",
            "Полезно для личных проектов (Useful for personal projects)"),
    myself("для себя",
            "Интересно послушать для себя (Interesting to myself)"),
    none("неинтересно",
            "Неинтересно (Not interested)"),
    ;

    private String match;
    private String description;

    Interest(String match, String description) {
        this.match = match;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getMatch() {
        return match;
    }

    public static Interest fromString(String value) {
        if (value == null) {
            return null;
        }

        for (Interest interest: Interest.values()) {
            if (value.toLowerCase().contains(interest.getMatch())) {
                return interest;
            }
        }

        return null;
    }
}
