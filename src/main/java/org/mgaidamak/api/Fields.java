package org.mgaidamak.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import org.mgaidamak.decode.InstantJsonAdapter;
import org.mgaidamak.decode.InterestJsonAdapter;
import org.mgaidamak.decode.RatingJsonAdapter;

import java.time.Instant;

public class Fields {
    @SerializedName("ID")
    @Expose
    private int id;

    @SerializedName("Org")
    @Expose
    @JsonAdapter(RatingJsonAdapter.class)
    private Integer org;

    @SerializedName("Rating")
    @Expose
    @JsonAdapter(RatingJsonAdapter.class)
    private Integer rating;

    @SerializedName("Interest")
    @Expose
    @JsonAdapter(InterestJsonAdapter.class)
    private Interest[] interest;

    @SerializedName("Level")
    @Expose
    @JsonAdapter(RatingJsonAdapter.class)
    private Integer level;

    @SerializedName("English")
    @Expose
    private boolean english;

    @SerializedName("More")
    @Expose
    private boolean more;

    @SerializedName("Video")
    @Expose
    private boolean video;

    @SerializedName("Email")
    @Expose
    private String email;

    @SerializedName("Note")
    @Expose
    private String note;

    @SerializedName("Date")
    @Expose
    @JsonAdapter(InstantJsonAdapter.class)
    private Instant date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getOrg() {
        return org;
    }

    public void setOrg(Integer org) {
        this.org = org;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public void setInterest(Interest[] interest) {
        this.interest = interest;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnglish() {
        return english;
    }

    public void setEnglish(boolean english) {
        this.english = english;
    }

    public boolean isMore() {
        return more;
    }

    public void setMore(boolean more) {
        this.more = more;
    }

    public boolean isVideo() {
        return video;
    }

    public void setVideo(boolean video) {
        this.video = video;
    }

    public Interest[] getInterest() {
        return interest;
    }

    public Integer getLevel() {
        return level;
    }

    public Instant getDate() {
        return date;
    }
}
