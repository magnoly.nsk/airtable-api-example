package org.mgaidamak.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Results {
    @SerializedName("offset")
    @Expose
    private String offset;

    @SerializedName("records")
    @Expose
    private Record[] records;

    public String getOffset() {
        return offset;
    }

    public Record[] getRecords() {
        return records;
    }
}
